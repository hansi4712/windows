# LibreWolf for windows

* **[download latest release](https://gitlab.com/librewolf-community/browser/windows/-/releases)**
* or install via [chocolatey](https://community.chocolatey.org/packages/librewolf): `choco install librewolf`
* **If your LibreWolf crashes on startup**, you probably miss the right [Visual C++ Runtime](https://support.microsoft.com/en-us/topic/the-latest-supported-visual-c-downloads-2647da03-1eea-4433-9aff-95f26a218cc0). You want the _Visual Studio 2015, 2017 and 2019_ version for **x64**, which would be **[this file](https://aka.ms/vs/16/release/vc_redist.x64.exe)**.

# Settings repository

* [[settings repository](https://gitlab.com/librewolf-community/settings)] - all _librewolf.cfg_ stuff.. _([issues](https://gitlab.com/librewolf-community/settings/-/issues))_

# Community links

* If at all possible, pleasse go to the relevant 'issues' link and start discussing thing there.
* [[reddit](https://www.reddit.com/r/LibreWolf/)] - [r/LibreWolf](https://www.reddit.com/r/LibreWolf/) 😺
* [[gitter](https://gitter.im/librewolf-community/librewolf)], and the same room on [matrix](https://app.element.io/#/room/#librewolf-community_librewolf:gitter.im) (element.io).
